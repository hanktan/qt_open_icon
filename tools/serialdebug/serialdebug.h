﻿#ifndef SERIALDEBUG_H
#define SERIALDEBUG_H

#include <QWidget>
#include "platformhead.h"
#ifdef QT_GREATER_NEW
#include <QSerialPort>
#include <QSerialPortInfo>
#else
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#endif

namespace Ui {
class SerialDebug;
}

class SerialDebug : public QWidget
{
    Q_OBJECT

public:
    explicit SerialDebug(QWidget *parent = 0);
    ~SerialDebug();

private:
    Ui::SerialDebug *ui;
    QSerialPort *serial;  //串口设备
    QTimer *timer;
    int recvNum;
    int sendNum;
    QString m_stamp = QString("<p style='color:gray;'>%1#%2 %3 >></p>");

    void initGui();
    void setSerialAttr();
    void sendSerialData(char * data, int length);

private slots:
    void errHandler(QSerialPort::SerialPortError);
    void on_openBtn_clicked();
    void recvSlot();
    void on_sendBtn_clicked();
    void on_sendEmptyBtn_clicked();
    void on_recvEmptyBtn_clicked();
    void timerSlot();
    void on_cycleSendStopBtn_clicked();
    void on_sendCycleCheckBox_clicked(bool checked);
    void on_againScanBtn_clicked();
    void on_sendHexCheckBox_clicked(bool checked);
    void on_resetNumBtn_clicked();
};

#endif // SERIALDEBUG_H
