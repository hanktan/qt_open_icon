#include "mybuttontest.h"
#include "ui_mybuttontest.h"

MyButtonTest::MyButtonTest(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyButtonTest)
{
    ui->setupUi(this);

    //
    ui->widget_1->setDisabledColor(QColor(0, 0, 0));
    ui->widget_1->setCheckedColor(QColor(255, 0, 0));
    ui->widget_1->setText("Hello World");

    ui->widget_2->setDisabledColor(QColor(0, 0, 0));
    ui->widget_2->setCheckedColor(QColor(255, 0, 0));
    ui->widget_2->setText("Hello World");

    ui->widget_3->setLabelPosition(Checkable::LabelPositionLeft);
    ui->widget_3->setDisabledColor(QColor(0, 0, 0));
    ui->widget_3->setCheckedColor(QColor(255, 0, 0));
    ui->widget_3->setText("Hello World");

    ui->widget_4->setLabelPosition(Checkable::LabelPositionLeft);
    ui->widget_4->setDisabledColor(QColor(0, 0, 0));
    ui->widget_4->setCheckedColor(QColor(255, 0, 0));
    ui->widget_4->setText("Hello World");

    //
    ui->widget_5->setDisabledColor(QColor(0, 0, 0));
    ui->widget_5->setCheckedColor(QColor(255, 0, 0));
    ui->widget_5->setText("Hello World");

    ui->widget_6->setDisabledColor(QColor(0, 0, 0));
    ui->widget_6->setCheckedColor(QColor(255, 0, 0));
    ui->widget_6->setText("Hello World");

    ui->widget_7->setLabelPosition(Checkable::LabelPositionLeft);
    ui->widget_7->setDisabledColor(QColor(0, 0, 0));
    ui->widget_7->setCheckedColor(QColor(255, 0, 0));
    ui->widget_7->setText("Hello World");

    ui->widget_8->setLabelPosition(Checkable::LabelPositionLeft);
    ui->widget_8->setDisabledColor(QColor(0, 0, 0));
    ui->widget_8->setCheckedColor(QColor(255, 0, 0));
    ui->widget_8->setText("Hello World");

    //
    ui->widget_9->setDisabledColor(QColor(0, 0, 0));
    ui->widget_9->setActiveColor(QColor(255, 0, 0));
    ui->widget_9->setInactiveColor(QColor(0, 255, 0));
    ui->widget_9->setTrackColor(QColor(0, 0, 255));

    ui->widget_10->setDisabledColor(QColor(0, 0, 0));
    ui->widget_10->setActiveColor(QColor(255, 255, 0));
    ui->widget_10->setInactiveColor(QColor(0, 255, 255));
    ui->widget_10->setTrackColor(QColor(255, 0, 255));

    ui->widget_11->setOrientation(Qt::Vertical);
    ui->widget_11->setDisabledColor(QColor(0, 0, 0));
    ui->widget_11->setActiveColor(QColor(255, 0, 0));
    ui->widget_11->setInactiveColor(QColor(0, 255, 0));
    ui->widget_11->setTrackColor(QColor(0, 0, 255));

    ui->widget_12->setOrientation(Qt::Vertical);
    ui->widget_12->setDisabledColor(QColor(0, 0, 0));
    ui->widget_12->setActiveColor(QColor(255, 255, 0));
    ui->widget_12->setInactiveColor(QColor(0, 255, 255));
    ui->widget_12->setTrackColor(QColor(255, 0, 255));
}

MyButtonTest::~MyButtonTest()
{
    delete ui;
}
