#include "marqueelabeltest.h"
#include "ui_marqueelabeltest.h"

MarqueeLabelTest::MarqueeLabelTest(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MarqueeLabelTest)
{
    ui->setupUi(this);

    ui->widget->setText("Hello World!");
}

MarqueeLabelTest::~MarqueeLabelTest()
{
    delete ui;
}
