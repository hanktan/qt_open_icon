#ifndef LINEEDITWIDGETTEST_H
#define LINEEDITWIDGETTEST_H

#include <QWidget>

namespace Ui {
class LineEditWidgetTest;
}

class LineEditWidgetTest : public QWidget
{
    Q_OBJECT
public:
    explicit LineEditWidgetTest(QWidget *parent = nullptr);
    ~LineEditWidgetTest();

private:
    Ui::LineEditWidgetTest *ui;

};

#endif
