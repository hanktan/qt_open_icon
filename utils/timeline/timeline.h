﻿#ifndef TIMELINE_H
#define TIMELINE_H

#include <QtWidgets>

class TimeLine : public QWidget
{
    Q_OBJECT
public:
    explicit TimeLine(QWidget *parent = nullptr);

protected:
    virtual void paintEvent(QPaintEvent *e);
    virtual void mousePressEvent(QMouseEvent *e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void leaveEvent(QEvent *e);

private:
    void drawBackground(QPainter *painter);
    void drawGroove(QPainter *painter);
    void drawHandle(QPainter *painter);
    void drawTicks(QPainter *painter);
signals:
    void dateTimeSig(QDateTime);
public slots:
    void updateTime(QDateTime dateTime);

private:
    bool pressDown;
    bool isInside;
    QPoint pressPoint;
    QPoint offset;
    QDateTime dateTimeStart;
    QPoint mousePoint;
};

#endif // TIMELINE_H
