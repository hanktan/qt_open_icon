﻿/**
 ** @author:	   王世雄
 ** @date:        2019.3.20
 ** @brief:        进度状态条
 ** 1.strList 为进度状态条中的字符串列表
 ** 2.unfinishImg/finishImg 分别为状态改变前后的图片，线条会根据图片（10，10）位置的像素自动设置颜色
 ** 3.具有自适应功能
 */
#ifndef PROGRESSSTATE_H
#define PROGRESSSTATE_H

#include "platformhead.h"

class ProgressState : public QWidget
{
    Q_OBJECT

public:
    ProgressState(QVector<QString> strList, QString finishImg, QString unfinishImg, QWidget *parent = 0);
    ~ProgressState();
    void setMargin(int margin);
    void setIndexCount(int indexCount);

    int currentIndex() const;
    int getIndexCount();

public slots:
        void setCurrentIndex(int index);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    void initMembers();
    // 绘制
    void drawGroove(QPainter *painter);
    // 刷新
    void refreshContectsRect();
    void refreshPoints();

private:
    // 参数成员
    int margin;
    int width;
    int height;
    int indexCount;
    int index;
    QColor finishColor;
    QColor unfinishColor;

    // 状态成员
    QVector<QPointF> pointList;
    QVector<QString> strList;
    QRect rctContents;             // 内容Rect
    QImage *unfinishedImg;
    QImage *finishedImg;
    QVector<QRect> textRectList;
};

#endif // ProgressState_H
