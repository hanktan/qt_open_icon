﻿/**
 ** @author:	   浓咖啡
 ** @date:	   2016.6.23
 ** @brief:      消息对话框
 */
#include "dialogtest.h"
#include "fontawesomewebfont.h"
#include "gbk.h"
DialogTest::DialogTest()
{
    this->setWindowTitle(QString(a2w("消息对话框")));
    QVBoxLayout *lay = new QVBoxLayout(this);

    QPushButton *toastBtn = new QPushButton("toast");
    QPushButton *waningBtn = new QPushButton(a2w("确认消息框"));
    QPushButton *messageBtn = new QPushButton(a2w("提示框"));
    QPushButton *notifyBtn1 = new QPushButton(a2w("长通知"));
    QPushButton *notifyBtn2 = new QPushButton(a2w("短通知"));
    notifyBtn1->setObjectName("notifyl");
    notifyBtn2->setObjectName("notifys");

    lay->addWidget(toastBtn);
    lay->addWidget(waningBtn);
    lay->addWidget(messageBtn);
    lay->addWidget(notifyBtn1);
    lay->addWidget(notifyBtn2);

    connect(toastBtn, SIGNAL(released()), this, SLOT(toastSlot()));
    connect(waningBtn, SIGNAL(released()), this, SLOT(warningSlot()));
    connect(messageBtn, SIGNAL(released()), this, SLOT(messageSlot()));
    connect(notifyBtn1, SIGNAL(clicked()), this, SLOT(notifySlot()));
    connect(notifyBtn2, SIGNAL(clicked()), this, SLOT(notifySlot()));
    man = new NotifyManager(this);
    man->setMaxCount(5);
    setFixedSize(300, 300);
}

void DialogTest::toastSlot()
{
    Toast *w = new Toast(this, a2w("这是一个好的组件库"));
    w->toast();
}

void DialogTest::warningSlot()
{
    Toast *toast;
    QString qss = utilscommon::readFile(":/qss/resourse/qss/dialog/default.qss");
    BaseLabel *labIcon = new BaseLabel(FontawesomeWebfont(), FontawesomeWebfont::ICON_WAINING);
    BaseMessageBox *w = new BaseMessageBox(labIcon, a2w("警告"), a2w("确定删除？"), qss);
    switch (w->exec())
    {
    case BaseMessageBox::OK:
        toast = new Toast(this, a2w("选择了确定"));
        toast->toast();
        break;
    default:
        toast = new Toast(this, a2w("选择了取消"));
        toast->toast();
        break;
    }
}

void DialogTest::messageSlot()
{
    QString qss = utilscommon::readFile(":/qss/resourse/qss/dialog/default.qss");
    BaseLabel *labIcon = new BaseLabel(FontawesomeWebfont(), FontawesomeWebfont::ICON_INFO);
    BaseMessageBox *w = new BaseMessageBox(labIcon, a2w("提示"), a2w("这是个好的组件库"), qss, false);
    w->show();
}

void DialogTest::notifySlot()
{
    if(sender()->objectName()=="notifyl")
        man->notify(this, a2w("长通知"), a2w("可更新..."), 1000*10);
    else
        man->notify(this, a2w("短通知"), a2w("你有一条新消息"), 1000);
}
